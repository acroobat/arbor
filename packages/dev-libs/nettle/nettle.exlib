# Copyright 2011 Daniel Mierswa <impulze@impulze.org>
# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.gz ]
require alternatives

export_exlib_phases src_install

SUMMARY="A cryptographic library that is designed to fit easily in more or less any context"
HOMEPAGE="https://www.lysator.liu.se/~nisse/${PN}"

LICENCES="|| ( LGPL-2.1 LGPL-3 )"
SLOT="0"
MYOPTIONS="arm_cpu_features: neon"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/nettle.html [[ lang = en description = manual ]]"

if ever at_least 3.6 ; then
    GMP_VER=6.1
else
    GMP_VER=6.0
fi

DEPENDENCIES="
    build+run:
        dev-libs/gmp:=[>=${GMP_VER}] [[ note = [ for public key algorithms ] ]]
    run:
        !dev-libs/nettle:0[<3.5.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

# TODO:
# configure appends -ggdb3 unconditionally and it seems to be explicitly
# wanted by the author, check what problems arise once it's removed
DEFAULT_SRC_CONFIGURE_PARAMS=(
    CC_FOR_BUILD="$(exhost --build)-cc"
    --enable-public-key
    --enable-shared
    --disable-mini-gmp
    --disable-openssl # only used for benchmarking
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=( 'arm_cpu_features:neon arm-neon' )

if ever at_least 3.7 ; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --disable-fat
    )
fi

nettle_src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}-hash           ${PN}-hash-${SLOT}
        /usr/${host}/bin/${PN}-lfib-stream    ${PN}-lfib-stream-${SLOT}
        /usr/${host}/bin/${PN}-pbkdf2         ${PN}-pbkdf2-${SLOT}
        /usr/${host}/bin/pkcs1-conv           pkcs1-conv-${SLOT}
        /usr/${host}/bin/sexp-conv            sexp-conv-${SLOT}
        /usr/${host}/include/${PN}            ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.a           lib${PN}-${SLOT}.a
        /usr/${host}/lib/lib${PN}.so          lib${PN}-${SLOT}.so
        /usr/${host}/lib/libhogweed.a         libhogweed-${SLOT}.a
        /usr/${host}/lib/libhogweed.so        libhogweed-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc   ${PN}-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/hogweed.pc hogweed-${SLOT}.pc
    )

    other_alternatives+=(
        /usr/share/info/${PN}.info ${PN}-${SLOT}.info
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

